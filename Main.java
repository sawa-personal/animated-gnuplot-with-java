//*******************************************************
// Main.java
// created by Sawada Tatsuki on 2017/12/18.
// Copyright © 2017 Sawada Tatsuki. All rights reserved.
//*******************************************************
/* アニメーション付Gnuplotのプログラム */

public class Main extends AnimatedGnuplot {

    private static double w = 24; //刻み幅
    // 関数値を一点ずつプロット

    public void setting() {
		String plt = "set terminal pngcairo";
		query(plt); // アンチエイリアスpngで出力
    }

    public void setup() {
		highQuarity(true);
    }
    
    public void loop() {
		;
    }
    
    public static void main(String[] args){
		launch(args);
    }    
}
