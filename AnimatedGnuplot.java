//*******************************************************
// AnimatedGnuplot.java
// created by Sawada Tatsuki on 2017/12/18.
// Copyright © 2017 Sawada Tatsuki. All rights reserved.
//*******************************************************
/* アニメーションGnuplotの処理部分を書く */

import java.util.*;

import java.io.*;

import java.awt.image.BufferedImage;
import javax.imageio.ImageIO;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.EventHandler;
import javafx.event.ActionEvent;
import javafx.util.Duration;

import javafx.embed.swing.SwingFXUtils;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import javafx.scene.canvas.Canvas;
import javafx.scene.image.ImageView;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;

public abstract class AnimatedGnuplot extends Application {    
    public abstract void setting();
    public abstract void setup();
    public abstract void loop();

    private static final String GNUPLOT = "C:/Program Files/gnuplot/bin/gnuplot.exe"; //Gnuplotのパス
    private StackPane stackPane = new StackPane(); //stackPane
    private ImageView imageView = new ImageView(); //画像を表示する土台
    private Image image;  //グラフ画像
    protected void query(String plt){
    }

    protected void highQuarity(boolean q) {
		if(q == true){
			terminal = "pngcairo";
			refRate = 100;
		} else {
			terminal = "gif";
			refRate = 70;
		}
    }
    
    private double xmin = -20;
    private double xmax = 20;
    private double ymin = -20;
    private double ymax = 20;
    private double zmin = 0;
    private double zmax = 500;
    private double refRate;
    private String terminal;
    private double t = 0;    
    @Override public void start(Stage stage) throws IOException {
		Process p = new ProcessBuilder(GNUPLOT, "-").start();
		PrintWriter out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(p.getOutputStream())));
		BufferedInputStream in = new BufferedInputStream(p.getInputStream());
		highQuarity(false);
		setting(); //セッティング処理
		setup(); //セットアップ処理
		out.println("set ticslevel 0"); //
		out.println("set isosamples 28"); //線の細かさ
		out.println("set view 64, 16, 1, 1"); //前後に回転，左右に回転，全体の縮尺，z軸の縮尺
		out.println("set grid");
		out.println("set terminal " + terminal);    // アンチエイリアスpngで出力
		out.println("set xrange[" + xmin + ":" + xmax + "]");
		out.println("set yrange[" + ymin + ":" + ymax + "]");
		out.println("set zrange[" + zmin + ":" + zmax + "]");
		out.println("splot (x - 10)**2 + y**2 lc rgb '#140ce9' t 'obj'"); // writeData()のデータを描画
		out.println("set output"); // 画像を生成
		out.close(); // flushし，標準出力を閉じる
		image = SwingFXUtils.toFXImage(ImageIO.read(in), null); //ここで失敗してる
		imageView.setImage(image);
		in.close(); // 標準入力を閉じる

		stackPane.getChildren().add(imageView);
		Scene scene = new Scene(stackPane); //gridPaneをsceneに追加する
		stage.setTitle("Gnuplot"); //タイトルバーの文字を設定
		stage.setScene(scene); //stageにsceneをセット
		stage.show();// stageの描画

        Timeline timer = new Timeline(new KeyFrame(Duration.millis(refRate),new EventHandler<ActionEvent> (){
				@Override public void handle(ActionEvent event) {
					try {
						t+=5;
						if(t == 360) {
							t = 0;
						}
						Process p = new ProcessBuilder(GNUPLOT, "-").start();
						PrintWriter out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(p.getOutputStream())));
						BufferedInputStream in = new BufferedInputStream(p.getInputStream());
						//loop(); //ループ処理
						out.println("set ticslevel 0"); //
						out.println("set isosamples 28"); //線の細かさ
						out.println("set view 64, 16, 1, 1"); //前後に回転，左右に回転，全体の縮尺，z軸の縮尺
						out.println("set grid");
						out.println("set terminal " + terminal);    // アンチエイリアスpngで出力
						out.println("set xrange[" + xmin + ":" + xmax + "]");
						out.println("set yrange[" + ymin + ":" + ymax + "]");
						out.println("set zrange[" + zmin + ":" + zmax + "]");
						double deg = Math.PI / 180 * t;
						out.println("splot (x - 10 * cos(" + deg + "))**2 + (y - 10 * sin(" + deg + "))**2 lc rgb '#140ce9' t 't = " + t + "'"); // writeData()のデータを描画
						out.println("set output"); // 画像を生成
						out.close(); // flushし，標準出力を閉じる
						image = SwingFXUtils.toFXImage(ImageIO.read(in), null);
						imageView.setImage(image); 
						in.close(); // 標準入力を閉じる
						//set outputし，imageを生成し，シーンに代入
					} catch(Exception e) {System.out.println("error!!");}
				}
			}));
		timer.setCycleCount(Timeline.INDEFINITE);
		timer.play();
    }
}
